﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainsSolver
{
    public class Square
    {
        private byte x;
        private byte y;
        private Dictionary<BorderLocation, BorderNode> BorderDingJezusHoeVertaalJeDit;
        private Square Top;
        private Square Bottom;
        private Square Left;
        private Square Right;
        private Tile currentTile;

        private Square Neighbour(BorderLocation connectedToNode)
        {
            switch (connectedToNode)
            {
                case BorderLocation.BottomLeft:
                case BorderLocation.BottomRight:
                    return Bottom;
                case BorderLocation.LeftBottom:
                case BorderLocation.LeftTop:
                    return Left;
                case BorderLocation.RightBottom:
                case BorderLocation.RightTop:
                    return Right;
                case BorderLocation.TopLeft:
                case BorderLocation.TopRight:
                    return Top;
                default:
                    return new Square();
            }

        }

        public Path KeepWalking(BorderLocation exitNode, Path currentPath)
        {
            if (currentTile != null)
                return currentTile.GetPath(exitNode, currentPath);
            return currentPath;
        }

    }
}
