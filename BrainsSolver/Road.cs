﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainsSolver
{
    class Road
    {
        public BorderLocation EndPoint { get; private set; }
        public BorderSymbol Symbol { get; private set; }

        public BorderLocation NeighbourStartingPoint()
        {
            switch (EndPoint)
            {
                case BorderLocation.BottomLeft:
                    return BorderLocation.TopLeft;
                case BorderLocation.BottomRight:
                    return BorderLocation.TopRight;
                case BorderLocation.LeftBottom:
                    return BorderLocation.RightBottom;
                case BorderLocation.LeftTop:
                    return BorderLocation.RightTop;
                case BorderLocation.RightBottom:
                    return BorderLocation.LeftBottom;
                case BorderLocation.RightTop:
                    return BorderLocation.LeftTop;
                case BorderLocation.TopLeft:
                    return BorderLocation.BottomLeft;
                case BorderLocation.TopRight:
                    return BorderLocation.BottomRight;
                default:
                    throw new MissingMemberException();
            }
        }
    }
}
