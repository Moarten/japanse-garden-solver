﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainsSolver
{
    public class BorderNode
    {
        protected BorderSymbol Symbol;
        protected int Amount;

        public BorderNode()
        {
            Symbol = BorderSymbol.None;
            Amount = 1;
        }

        public BorderNode(BorderSymbol symbol, int amount)
        {
            Symbol = symbol;
            Amount = amount;
        }

        public bool InUse()
        {
            return Symbol != BorderSymbol.None;
        }
    }
}
