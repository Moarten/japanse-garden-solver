﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainsSolver
{
    public class Tile
    {
        private Dictionary<BorderLocation, Road> Nodes;
        private Square Slot;
        private Rotation CurrentRotation;

        public bool InUse()
        {
            return Slot != null;
        }

        public Tile()
        {
            CurrentRotation = Rotation.Original;
        }

        public void Place(Square newSlot)
        {
            Slot = newSlot;
        }

        public void RemoveFromField()
        {
            Slot = null;
        }

        public void RotateOnce()
        {
            var value = ((int)CurrentRotation + 2) % 6;
            CurrentRotation = (Rotation)value;
        }

        public void Rotate(Rotation rotation)
        {
            CurrentRotation = rotation;
        }

        public Path GetPath(BorderLocation startingPoint)
        {
            var currentPath = new Path();           
            return GetPath(startingPoint, currentPath);
        }

        public Path GetPath(BorderLocation startingPoint, Path currentPath)
        {
            var correctedStartingPoint = Helpers.GetRotatedLocation(startingPoint, CurrentRotation);
            var road = Nodes[correctedStartingPoint];
            currentPath.AddLength();
            currentPath.AddSpecial(road.Symbol);
            var neighbourStart = Helpers.GetRotatedLocation(road.NeighbourStartingPoint(), CurrentRotation);
            return Slot.KeepWalking(neighbourStart, currentPath);
        }
    }
}
