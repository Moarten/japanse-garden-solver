﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainsSolver
{
    public enum BorderLocation
    {
        TopLeft,
        TopRight,
        RightTop,
        RightBottom,
        BottomRight,
        BottomLeft,
        LeftBottom,
        LeftTop,
        Center
    }
    public enum BorderSymbol
    {
        None = 0,
        Tree = 1,
        Fire = 2,
        Butterfly = 4,
        YingYangGuoXiang = 8,
        Paraplu = 16,
        Bridge = 32,
        Tiles = 64
    }
    public enum Rotation
    {
        Original = 0,
        Left = 2,
        Bottom = 4,
        Right = 6
    }
}
