﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainsSolver
{
    public class Helpers
    {
        public static BorderLocation GetRotatedLocation(BorderLocation location, Rotation rotation)
        {
            var value = ((int)location + (int)rotation % 8);
            return (BorderLocation)value;
        }
    }
}
