﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainsSolver
{
    public class Path
    {
        private int Length;
        private BorderSymbol Symbols;

        public void AddLength()
        {
            Length++;
        }
        public void AddSpecial(BorderSymbol symbol)
        {
            Symbols |= symbol;
        }

        public Path()
        {

        }
        public Path(BorderSymbol symbol)
        {
            Symbols = symbol;
        }

    }
}
